// https://docs.cypress.io/api/introduction/api.html

describe("Calculator", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("computes", () => {
    cy.contains(".btn", 2).click();
    cy.contains(".btn", 3).click();
    cy.contains(".operator", "+").click();
    cy.contains(".btn", 1).click();
    cy.contains(".btn", 9).click();
    cy.contains(".operator", "=").click();
    cy.contains(".display", 42);
    cy.log("**division**");
    cy.contains(".operator", "÷").click();
    cy.contains(".btn", 2).click();
    cy.contains(".operator", "=").click();
    cy.contains(".display", 21);
    cy.log("**soustraction**");
    cy.contains(".btn", "C").click();
    cy.contains(".btn", 4).click();
    cy.contains(".operator", "-").click();
    cy.contains(".btn", 2).click();
    cy.contains(".operator", "=").click();
    cy.contains(".display", 2);
    cy.log("**multiplication**");
    cy.contains(".btn", "C").click();
    cy.contains(".btn", 9).click();
    cy.contains(".operator", "x").click();
    cy.contains(".btn", 2).click();
    cy.contains(".operator", "=").click();
    cy.contains(".display", 18);
  });
});
